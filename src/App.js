import "./App.css";
import Dashboard from "./Components/Dashboard/Dashboard";
import MainRouter from "./Components/Router/MainRoute";

function App() {
  return (
    <div>
      <MainRouter />
    </div>
  );
}

export default App;
