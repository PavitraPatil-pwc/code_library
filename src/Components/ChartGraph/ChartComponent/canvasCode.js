import React from "react";
import CanvasJSReact from "./canvasjs.react";
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

function Graph() {
  
  const options = {
    theme: "light2",
    animationEnabled: true,
    title: {
      text: "Units Sold VS Profit",
    },
    subtitles: [
      {
        text: "",
      },
    ],
    axisX: {
      title: "States",
    },
    axisY: {
      title: "Units Sold",
      titleFontColor: "#6D78AD",
      lineColor: "#6D78AD",
      labelFontColor: "#6D78AD",
      tickColor: "#6D78AD",
    },
    axisY2: {
      title: "Profit in USD",
      titleFontColor: "#51CDA0",
      lineColor: "#51CDA0",
      labelFontColor: "#51CDA0",
      tickColor: "#51CDA0",
    },
    toolTip: {
      shared: true,
    },
    legend: {
      cursor: "pointer",
    },
    data: [
      {
        type: "spline",
        name: "Units Sold",
        showInLegend: true,
        xValueFormatString: "MMM YYYY",
        yValueFormatString: "#,##0 Units",
        dataPoints: [
          { x: new Date(2021, 0, 1), y: 120 },
          { x: new Date(2021, 1, 1), y: 135 },
          { x: new Date(2021, 2, 1), y: 144 },
          { x: new Date(2021, 3, 1), y: 103 },
          { x: new Date(2021, 4, 1), y: 93 },
          { x: new Date(2021, 5, 1), y: 129 },
          { x: new Date(2021, 6, 1), y: 143 },
          { x: new Date(2021, 7, 1), y: 156 },
          { x: new Date(2021, 8, 1), y: 122 },
          { x: new Date(2021, 9, 1), y: 106 },
          { x: new Date(2021, 10, 1), y: 137 },
          { x: new Date(2021, 11, 1), y: 142 },
        ],
      },
      {
        type: "spline",
        name: "Profit",
        axisYType: "secondary",
        showInLegend: true,
        xValueFormatString: "MMM YYYY",
        yValueFormatString: "$#,##0.#",
        dataPoints: [
          { x: new Date(2021, 0, 1), y: 19034.5 },
          { x: new Date(2021, 1, 1), y: 20015 },
          { x: new Date(2021, 2, 1), y: 27342 },
          { x: new Date(2021, 3, 1), y: 20088 },
          { x: new Date(2021, 4, 1), y: 20234 },
          { x: new Date(2021, 5, 1), y: 29034 },
          { x: new Date(2021, 6, 1), y: 30487 },
          { x: new Date(2021, 7, 1), y: 32523 },
          { x: new Date(2021, 8, 1), y: 20234 },
          { x: new Date(2021, 9, 1), y: 27234 },
          { x: new Date(2021, 10, 1), y: 33548 },
          { x: new Date(2021, 11, 1), y: 32534 },
        ],
      },
    ],
  };

  return (
    <div>
      <CanvasJSChart options={options} />
    </div>
  );
}

export default Graph;
