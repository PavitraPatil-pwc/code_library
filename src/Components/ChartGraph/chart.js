import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import { CardContent } from "@mui/material";
import Card from "@mui/material/Card";
import Graph1 from "../Assets/Graph1.png";
import Graph2 from "../Assets/Graph2.png";
import Graph3 from "../Assets/Graph3.png";
import Graph4 from "../Assets/Graph4.png";
import Graph from "./ChartComponent/canvasCode";

const ChartGraph = () => {
  return (
    <div>
      <Typography variant="h5" component="div">
        Chart And Graph
      </Typography>
      <Typography component="div" style={{ margin: 8 }}>
        React Chart Component supports Area, Spline Area, Stacked Area, Stacked
        Area 100, Step Area, Range Area and Range Spline Area Charts.
      </Typography>
      <Typography
        variant="h6"
        component="div"
        style={{ borderBottom: "1px solid grey", marginBottom: 20 }}
      >
        Layout
      </Typography>
      <Box sx={{ flexGrow: 1 }} style={{ marginTop: 20, marginBottom: 10 }}>
        <Grid container style={{ marginBottom: 10 }}>
          <Grid item xs={10}>
            <img src={Graph1} alt="Graph1" height="300" width="850" />
          </Grid>
        </Grid>
        <Grid container style={{ marginBottom: 10 }}>
          <Grid item xs={10}>
            <img src={Graph2} alt="Graph2" height="300" width="850" />
          </Grid>
        </Grid>
        <Grid container style={{ marginBottom: 10 }}>
          <Grid item xs={10}>
            <img src={Graph3} alt="Graph3" height="300" width="850" />
          </Grid>
        </Grid>
        <Grid container style={{ marginBottom: 10 }}>
          <Grid item xs={10}>
            <img src={Graph4} alt="Graph4" height="300" width="850" />
          </Grid>
        </Grid>
      </Box>
      <Typography component="div" style={{ marginBottom: 20 }}>
        React Charts {"&"} Graphs Component with 10x Performance for Web
        Applications. Charts are interactive, responsive and support animation,
        zooming, panning, events, exporting chart as image, drilldown {"&"}
        real-time updates. React Chart library comes with 30+ chart types
        including line, column, bar, pie, doughnut, range charts, stacked
        charts, stock charts, etc. With these high performing charts, you can
        add hundreds of thousands of data points without causing performance
        lag.
      </Typography>
      <Typography
        variant="h6"
        component="div"
        style={{ borderBottom: "1px solid grey", marginBottom: 20 }}
      >
        Default
      </Typography>{" "}
      <Typography component="div" style={{ marginBottom: 20 }}>
        React Chart component supports rendering multiple axes with different
        scales on either sides. This is useful while rendering multi-series
        chart with different dimensions / range of values. Below example shows
        line chart with multiple axes alongside source code that you can run
        locally.
      </Typography>
      <Card style={{ marginBottom: 20, background: "grey" }}>
        <CardContent>
          <Graph />
        </CardContent>
      </Card>
      <Typography
        variant="h6"
        component="div"
        style={{ borderBottom: "1px solid grey", marginBottom: 20 }}
      >
        Usage
      </Typography>
      <Card style={{ marginBottom: 20 }}>
        <CardContent>
          <Typography
            variant="h6"
            component="div"
            style={{ borderBottom: "1px solid grey", marginBottom: 20 }}
          >
            App.js
          </Typography>
          <pre>
            <span>
              import React, {"{"} Component {"}"} from 'react';
            </span>
            <br />
            <span>import CanvasJSReact from './canvasjs.react';</span>
            <br />
            <span>var CanvasJS = CanvasJSReact.CanvasJS;</span>
            <br />
            <span>var CanvasJSChart = CanvasJSReact.CanvasJSChart;</span>
            <br />
            <span>class App extends Component {"{"}</span>
            <br />
            <span>
              constructor{"()"}
              {" {"}
            </span>
            <br />
            <span>super{"()"};</span>
            <br />
            <span>
              this.toggleDataSeries = this.toggleDataSeries.bind{"("}this{")"};
            </span>
            <br />
            <span>{"}"}</span>
            <br />
            <span>
              toggleDataSeries{"(e)"}
              {"{"}
            </span>
            <br />
            <span>
              if {"("}typeof{"("}e.dataSeries.visible{")"} === "undefined" ||
              e.dataSeries.visible{")"} {"{"}
            </span>
            <br />
            <span>e.dataSeries.visible = false;</span>
            <br />
            <span>{" }"}</span>
            <br />
            <span>else{"{"}</span>
            <br />
            <span>e.dataSeries.visible = true;</span>
            <br />
            <span>{"}"}</span>
            <br />
            <span>this.chart.render{"()"};</span>
            <br />
            <span>{"}"}</span>
            <br />
            <span>
              render{"()"} {"{"}
            </span>
            <br />
            <span>theme: "light2",</span>
            <br />
            <span>animationEnabled: true,</span>
            <br />
            <span>title:{"{"}</span>
            <br />
            <span>text: "Units Sold VS Profit"</span>
            <br />
            <span>{"}"},</span>
            <br />
            <span>text: "Click Legend to Hide or Unhide Data Series"</span>
            <br />
            <span>{"}],"}</span>
            <br />
            <span>axisX: {"{"}</span>
            <br />
            <span>title: "States"</span>
            <br />
            <span>{"},"}</span>
            <br />
            <span>axisY: {"{"}</span>
            <br />
            <span>title: "Units Sold",</span>
            <br />
            <span>titleFontColor: "#6D78AD",</span>
            <br />
            <span>lineColor: "#6D78AD",</span>
            <br />
            <span>labelFontColor: "#6D78AD",</span>
            <br />
            <span>tickColor: "#6D78AD"</span>
            <br />
            <span> {"},"}</span>
            <br />
            <span>title: "Profit in USD",</span>
            <br />
            <span>titleFontColor: "#51CDA0",</span>
            <br />
            <span>lineColor: "#51CDA0",</span>
            <br />
            <span>labelFontColor: "#51CDA0",</span>
            <br />
            <span>tickColor: "#51CDA0"</span>
            <br />
            <span>{"},"}</span>
            <br />
            <span>toolTip: {"{"}</span>
            <br />
            <span>shared: true</span>
            <br />
            <span> {"},"}</span>
            <br />
            <span>legend: {"{"}</span>
            <br />
            <span>cursor: "pointer",</span>
            <br />
            <span>itemclick: this.toggleDataSeries</span>
            <br />
            <span>{"},"}</span>
            <br />
            <span>data: {"[{"}</span>
            <br />
            <span>type: "spline",</span>
            <br />
            <span>name: "Units Sold",</span>
            <br />
            <span>showInLegend: true,</span>
            <br />
            <span>xValueFormatString: "MMM YYYY",</span>
            <br />
            <span>yValueFormatString: "#,##0 Units",</span>
            <br />
            <span>dataPoints: {"["}</span>
            <br />
            <span>
              {"{"} x: new Date{"("}2017, 0, 1{")"}, y: 120 {"}"},
            </span>
            <br />
            <span>
              {"{"} x: new Date{"("}2017, 1, 1{")"}, y: 135 {"}"},
            </span>
            <br />
            <span>
              {"{"} x: new Date{"("}2017, 2, 1{")"}, y: 144 {"}"},
            </span>
            <br />
            <span>
              {"{"} x: new Date{"("}2017, 3, 1{")"}, y: 130 {"}"},
            </span>
            <br />
            <span>
              {"{"} x: new Date{"("}2017, 4, 1{")"}, y: 93 {"}"},
            </span>
            <br />
            <span>
              {"{"} x: new Date{"("}2017, 5, 1{")"}, y: 129 {"}"},
            </span>
            <br />
            <span>
              {"{"} x: new Date{"("}2017, 6, 1{")"}, y: 143 {"}"},
            </span>
            <br />
            <span>
              {"{"} x: new Date{"("}2017, 7, 1{")"}, y: 156 {"}"},
            </span>
            <br />
            <span>
              {"{"} x: new Date{"("}2017, 8, 1{")"}, y: 122 {"}"},
            </span>
            <br />
            <span>
              {"{"} x: new Date{"("}2017, 9, 1{")"}, y: 106 {"}"},
            </span>
            <br />
            <span>
              {"{"} x: new Date{"("}2017, 10, 1{")"}, y: 137 {"}"},
            </span>
            <br />
            <span>
              {"{"} x: new Date{"("}2017, 11, 1{")"}, y: 142 {"}"},
            </span>
            <br />
            <span>{"]},"}</span>
            <br />
            <span>{"{"}</span>
            <br />
            <span>type: "spline",</span>
            <br />
            <span>name: "Profit",</span>
            <br />
            <span>axisYType: "secondary",</span>
            <br />
            <span>showInLegend: true,</span>
            <br />
            <span>xValueFormatString: "MMM YYYY",</span>
            <br />
            <span>yValueFormatString: "$#,##0.#",</span>
            <br />
            <span>dataPoints: {"["}</span>
            <br />
            <span>
              {"{"} x: new Date{"("}2017, 0, 1{")"}, y: 19034.5 {"}"},
            </span>
            <br />
            <span>
              {"{"} x: new Date{"("}2017, 1, 1{")"}, y: 20015 {"}"},
            </span>
            <br />
            <span>
              {"{"} x: new Date{"("}2017, 2, 1{")"}, y: 27342 {"}"},
            </span>
            <br />
            <span>
              {"{"} x: new Date{"("}2017, 3, 1{")"}, y: 20088 {"}"},
            </span>
            <br />
            <span>
              {"{"} x: new Date{"("}2017, 4, 1{")"}, y: 20234 {"}"},
            </span>
            <br />
            <span>
              {"{"} x: new Date{"("}2017, 5, 1{")"}, y: 29034 {"}"},
            </span>
            <br />
            <span>
              {"{"} x: new Date{"("}2017, 6, 1{")"}, y: 30487 {"}"},
            </span>
            <br />
            <span>
              {"{"} x: new Date{"("}2017, 7, 1{")"}, y: 32523 {"}"},
            </span>
            <br />
            <span>
              {"{"} x: new Date{"("}2017, 8, 1{")"}, y: 20234 {"}"},
            </span>
            <br />
            <span>
              {"{"} x: new Date{"("}2017, 9, 1{")"}, y: 27234 {"}"},
            </span>
            <br />
            <span>
              {"{"} x: new Date{"("}2017, 10, 1{")"}, y: 33548 {"}"},
            </span>
            <br />
            <span>
              {"{"} x: new Date{"("}2017, 11, 1{")"}, y: 32534 {"}"},
            </span>
            <br />
            <span>{"]"}</span>
            <br />
            <span>{"}]"}</span>
            <br />
            <span>{"}"}</span>
            <br />
            <span>return {"("}</span>
            <br />
            <span>{"<div>"}</span>
            <br />
            <span>{"<CanvasJSChart options = {options}"}</span>
            <br />
            <span>{"onRef={ref => this.chart = ref}"}</span>
            <br />
            <span>{"/>"}</span>
            <br />
            <span>{"</div>"}</span>
            <br />
            <span>{");"}</span>
            <br />
            <span>{"}"}</span>
            <br />
            <span>{"}"}</span>
            <br />
            <span>module.exports = App; </span>
          </pre>
        </CardContent>
      </Card>
    </div>
  );
};
export default ChartGraph;
