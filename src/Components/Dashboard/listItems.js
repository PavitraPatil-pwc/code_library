import * as React from "react";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import DashboardIcon from "@mui/icons-material/Dashboard";
import PeopleIcon from "@mui/icons-material/People";
import BarChartIcon from "@mui/icons-material/BarChart";
import DateRangeIcon from "@mui/icons-material/DateRange";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import ViewQuiltIcon from "@mui/icons-material/ViewQuilt";
import ArrowDownwardIcon from "@mui/icons-material/ArrowDownward";
import TimelineIcon from "@mui/icons-material/Timeline";
import GradientIcon from "@mui/icons-material/Gradient";
import TextFieldsIcon from "@mui/icons-material/TextFields";
import ContactPageIcon from "@mui/icons-material/ContactPage";
import { Link } from "react-router-dom";

export const mainListItems = (
  <React.Fragment>
    <Link to="/" style={{ textDecoration: "none" }}>
      <ListItemButton>
        <ListItemIcon>
          <DashboardIcon />
        </ListItemIcon>
        <ListItemText primary="Dashboard" />
      </ListItemButton>
    </Link>
    <Link to="/date" style={{ textDecoration: "none" }}>
      <ListItemButton>
        <ListItemIcon>
          <DateRangeIcon />
        </ListItemIcon>
        <ListItemText primary="Datepicker" />
      </ListItemButton>
    </Link>
    <Link to="/dropdown" style={{ textDecoration: "none" }}>
      <ListItemButton>
        <ListItemIcon>
          <ArrowDropDownIcon />
        </ListItemIcon>
        <ListItemText primary="Dropdown" />
      </ListItemButton>
    </Link>
    <Link to="/materialui" style={{ textDecoration: "none" }}>
      <ListItemButton>
        <ListItemIcon>
          <GradientIcon />
        </ListItemIcon>
        <ListItemText primary="Material UI" />
      </ListItemButton>
    </Link>
    <Link to="/accordian" style={{ textDecoration: "none" }}>
      <ListItemButton>
        <ListItemIcon>
          <ArrowDownwardIcon />
        </ListItemIcon>
        <ListItemText primary="Accordian" />
      </ListItemButton>
    </Link>
    <Link to="/chart" style={{ textDecoration: "none" }}>
      <ListItemButton>
        <ListItemIcon>
          <TimelineIcon />
        </ListItemIcon>
        <ListItemText primary="Chart And Graph" />
      </ListItemButton>
    </Link>
    <Link to="/autocompletetextfield" style={{ textDecoration: "none" }}>
      <ListItemButton>
        <ListItemIcon>
          <TextFieldsIcon />
        </ListItemIcon>
        <ListItemText primary="Autocomplete Textfield" />
      </ListItemButton>
    </Link>
    <Link to="/paginator" style={{ textDecoration: "none" }}>
      <ListItemButton>
        <ListItemIcon>
          <ContactPageIcon />
        </ListItemIcon>
        <ListItemText primary="Paginator" />
      </ListItemButton>
    </Link>
  </React.Fragment>
);
