import * as React from "react";
import { mainListItems } from "./listItems";
import Cards from "../Cards/Cards";
import Typography from "@mui/material/Typography";

export default function Dashboard() {
  return (
    <Typography>
      <Typography variant="h5" component="div">
        Introduction
      </Typography>
      <Typography component="div">
        This page is an overview of the React documentation and related
        resources.
      </Typography>
    </Typography>
  );
}
