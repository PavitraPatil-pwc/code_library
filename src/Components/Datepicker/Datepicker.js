import Typography from "@mui/material/Typography";
import Cards from "../Cards/Cards";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import MultiDate1 from "../Assets/Multi2.png";
import MultiDatebydate from "../Assets/Multi3.png";
import MultiDateRange from "../Assets/Range2.png";
import MultiDate1TillRange from "../Assets/Range3.png";
import DatepickerCode from "./DatepickerCode";
import { CardContent } from "@mui/material";
import Card from "@mui/material/Card";

const Datepicker = () => {
  return (
    <div>
      <Typography variant="h5" component="div">
        Datepicker
      </Typography>
      <Typography component="div" style={{ margin: 8 }}>
        Simple React datepicker component calendars with the ability to select
        the date in single, multiple and range modes.
      </Typography>
      <Typography
        variant="h6"
        component="div"
        style={{ borderBottom: "1px solid grey", marginBottom: 20 }}
      >
        Installation
      </Typography>
      <Typography style={{ marginBottom: 30 }}>
        <Cards container="npm install react-multi-date-picker" />
      </Typography>
      <Typography
        variant="h6"
        component="div"
        style={{ borderBottom: "1px solid grey", marginBottom: 20 }}
      >
        Layout
      </Typography>
      <Box sx={{ flexGrow: 1 }} style={{ marginTop: 20, marginBottom: 10 }}>
        <Grid container spacing={2}>
          <Grid item xs={3}>
            <img src={MultiDate1} alt="datepicker" height="200" width="200" />
          </Grid>
          <Grid item xs={3}>
            <img
              src={MultiDatebydate}
              alt="datepicker"
              height="200"
              width="200"
            />
          </Grid>
          <Grid item xs={3}>
            <img
              src={MultiDateRange}
              alt="datepicker"
              height="200"
              width="200"
            />
          </Grid>
          <Grid item xs={3}>
            <img
              src={MultiDate1TillRange}
              alt="datepicker"
              height="200"
              width="200"
            />
          </Grid>
        </Grid>
      </Box>
      <Typography component="div" style={{ marginBottom: 20 }}>
        You’ll need to install React and PropTypes separately since those
        dependencies aren’t included in the package. If you need to use a locale
        other than the default en-US, you'll also need to import that into your
        project from date-fns (see Localization section below). Below is a
        simple example of how to use the Datepicker in a React view. You will
        also need to require the CSS file from this package (or provide your
        own). The example below shows how to include the CSS from this package
        if your build system supports requiring CSS files (Webpack is one that
        does).
      </Typography>
      <Typography
        variant="h6"
        component="div"
        style={{ borderBottom: "1px solid grey", marginBottom: 20 }}
      >
        Default
      </Typography>{" "}
      <Typography component="div" style={{ marginBottom: 20 }}>
        The user can select multiple dates.
      </Typography>
      <Card style={{ marginBottom: 20, background: "grey" }}>
        <CardContent>
          <DatepickerCode />
        </CardContent>
      </Card>
      <Typography
        variant="h6"
        component="div"
        style={{ borderBottom: "1px solid grey", marginBottom: 20 }}
      >
        Usage
      </Typography>
      <Card style={{ marginBottom: 20 }}>
        <CardContent>
          <pre>
            <span>import React,{"{useState}"} from "react";</span>
            <br />
            <span>import DatePicker from "react-multi-date-picker";</span>
            <br />
            <span>export default function Example() {"{"}</span>
            <br />
            <span>const [value, setValue] = useState(new Date());</span>
            <br />
            <span>
              return {"<"}DatePicker value={"{"}value{"}"} onChange={"{"}
              setValue{"}"} /{">"};
            </span>
            <br />
            <span>{"}"}</span>
          </pre>
        </CardContent>
      </Card>
    </div>
  );
};
export default Datepicker;
