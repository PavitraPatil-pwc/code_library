import React, { useState } from "react";
import DatePicker, { getAllDatesInRange } from "react-multi-date-picker";
import DatePanel from "react-multi-date-picker/plugins/date_panel";
import "./DatepickerCode.css";

const format = "DD/MM/YYYY";

export default function DatepickerCode() {
  const [dates, setDates] = useState([]);

  return (
    <div className="App">
      {/* MULTI DATE PICKER STARTS */}
      <div style={{ textAlign: "left", marginBottom: "2rem" }}>
        <h5>Multi Date Picker</h5>
        <DatePicker
          value={dates}
          onChange={setDates}
          multiple
          sort
          placeholder="Select dates"
          format={format}
          calendarPosition="bottom-center"
          plugins={[<DatePanel />]}
        />
      </div>
      {/* MULTI DATE PICKER ENDS */}
      <hr />
      {/* DATE RANGE PICKER STARTS */}
      <div style={{ textAlign: "left", marginBottom: "4rem" }}>
        <h5>Date Range Picker</h5>
        <DatePicker
          range
          eachDaysInRange
          placeholder="Select start & end date"
          format={format}
          calendarPosition="bottom-left"
          onChange={(dateObjects) => {
            let allDates = getAllDatesInRange(dateObjects, true);
            console.log(allDates);
          }}
        />
      </div>
      {/* DATE RANGE PICKER ENDS */}
    </div>
  );
}
