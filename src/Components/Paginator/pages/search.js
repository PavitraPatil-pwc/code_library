import { useState, useEffect } from 'react';
import Paginator from '../components/paginator/paginator';
//import personList from '../assets/personList.json';
import './search.css';

const Search = () => {
    const [currentPage, setCurrentPage] = useState(1);
    const [totalPages, setTotalPages] = useState(1);
    const [totalpeopleCount, setTotalpeopleCount] = useState([]);
    const [people, setPeople] = useState([]);
    const leadsPerPage = 6;

    useEffect(() => {
        fetch('./assets/personList.json', {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }

        })
            .then((res) => res.json())
            .then((data) => {
                setTotalpeopleCount(data);
                setPeople(data.slice((currentPage - 1) * leadsPerPage, currentPage * leadsPerPage));
                const count = (data.length) / leadsPerPage;
                if (count > 1) {
                    setTotalPages(Number.isInteger(count) ? count :
                        Math.trunc(count) + 1);
                }
            });
    }, []);

    const handlePaginatorChange = (newPageNumber) => {
        setCurrentPage(newPageNumber);
        newPageNumber === totalPages ? setPeople(totalpeopleCount.
            slice((newPageNumber - 1) * leadsPerPage, totalpeopleCount.length)) :
            setPeople(totalpeopleCount.slice((newPageNumber - 1) * leadsPerPage,
                newPageNumber * leadsPerPage));
    }

    return <>
    <h1>User List</h1>
    <div className="parentContainer">
        <div className="personContainer">
            {people.length > 0 && people.map((p, i) => {
                return (
                    <div key={`personItem${i}`} className="personItem">
                        <img src={p.Image} />
                        <div key={`name${i}`}>{p.name}</div>
                        <div key={`phone${i}`}>{p.phone}</div>
                        <div key={`address${i}`}>{p.Address}</div>
                    </div>
                )
            })}
        </div>
        <Paginator
            currentPage={currentPage}
            totalPages={totalPages}
            handleChange={handlePaginatorChange}
        />
    </div></>;
}

export default Search;
