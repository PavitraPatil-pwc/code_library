import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import { CardContent } from "@mui/material";
import Card from "@mui/material/Card";
import Cards from "../Cards/Cards";
import paginator from "../Assets/Paginator.png";
import Search from "./pages/search";

const PaginatorPage = () => {
  return (
    <div>
      <Typography variant="h5" component="div">
        Paginator
      </Typography>
      <Typography component="div" style={{ margin: 8 }}>
        The Paginator component can be used when we are getting 50+ records from
        an API, but want to show only 10-15 records at a time.
      </Typography>
      <Typography
        variant="h6"
        component="div"
        style={{ borderBottom: "1px solid grey", marginBottom: 20 }}
      >
        Layout
      </Typography>
      <Box sx={{ flexGrow: 1 }} style={{ marginTop: 20, marginBottom: 10 }}>
        <Grid container style={{ marginBottom: 10 }}>
          <Grid item xs={10}>
            <img src={paginator} alt="paginator" height="400" width="850" />
          </Grid>
        </Grid>
      </Box>
      <Typography component="div" style={{ marginBottom: 20 }}>
        Each paginator instance requires: The number of items per page {"("}
        default set to 50{")"} The total number of items being paged The current
        page index defaults to 0, but can be explicitly set via pageIndex. When
        the user interacts with the paginator, a PageEvent will be fired that
        can be used to update any associated data view.
      </Typography>
      <Typography
        variant="h6"
        component="div"
        style={{ borderBottom: "1px solid grey", marginBottom: 20 }}
      >
        Default
      </Typography>{" "}
      <Card
        style={{ marginBottom: 20, background: "grey", alignSelf: "center" }}
      >
        <CardContent>
          <Search />
        </CardContent>
      </Card>{" "}
      <Typography
        variant="h6"
        component="div"
        style={{ borderBottom: "1px solid grey", marginBottom: 20 }}
      >
        Instruction
      </Typography>
      <Typography component="div" style={{ marginBottom: 20 }}>
        <pre>
          <span>To use this component in your application :-</span>
          <br />
          <span>1. Import the Paginator component in your page.</span>
          <br />
          <span>
            2. The Paginator requires 3 parameters to work-: currentPage,
            totalPages, handleChange{"()"}
          </span>
          <br />
          <span>
            3. totalPages can be calculated by "number of records/number of
            records to be displayed at a time.
          </span>
          <br />
          <span>
            {"   "}
            In the shared code number of records= 46
          </span>
          <br />
          <span>
            {"   "}
            records to be displayed at a time= 6.
          </span>
          <br />
          <span>
            4. currentPage is initially 1 and it increass/decreases depending on
          </span>
          <br />
          <span>
            {"   "}
            previous/next button click in the paginator.
          </span>
          <br />
          <span>
            5. handleChange() -{">"} sets the next set of records to be shown.
          </span>
        </pre>
      </Typography>
      <Typography
        variant="h6"
        component="div"
        style={{ borderBottom: "1px solid grey", marginBottom: 20 }}
      >
        Usage
      </Typography>
      <Card style={{ marginBottom: 20 }}>
        <CardContent>
          <Typography
            variant="h6"
            component="div"
            style={{ borderBottom: "1px solid grey", marginBottom: 20 }}
          >
            App.js
          </Typography>
          <pre>
            <span>
              import {"{"} useState, useEffect {"}"} from 'react';
            </span>
            <br />
            <span>import Paginator from './paginator';</span>
            <br />
            <span>
              const Search ={"()"} ={">"} {"{"}
            </span>
            <br />
            <span>{"const [currentPage, setCurrentPage] = useState(1);"}</span>
            <br />
            <span>{"const [totalPages, setTotalPages] = useState(1);"},</span>
            <br />
            <span>{"options = [],"},</span>
            <br />
            <span>
              {"const [totalpeopleCount, setTotalpeopleCount] = useState([]);"}
            </span>
            <br />
            <span>{"const [people, setPeople] = useState([]);"}</span>
            <br />
            <span>{"const leadsPerPage = 6;"}</span>
            <br />
            <span>{"useEffect(() => {"}</span>
            <span>
              fetch{"("}'./assets/personList.json',{" {"}
            </span>
            <br />
            <span>
              {"<"}p{">"},{"{"}label{"}"};
            </span>
            <br />
            <span>{" headers: {"}</span>
            <br />
            <span>headers: {"{"}</span>
            <br />
            <span>'Content-Type': 'application/json',</span>
            <br />
            <span>'Accept': 'application/json'</span>
            <br />
            <span>{"}"}</span>
            <br />
            <span>{"})"}</span>
            <br />
            <span>{".then((res) => res.json())"}</span>
            <br />
            <span>{".then((data) => {"}</span>
            <br />
            <span>
              {"<"}div{">"}
            </span>
            <br />
            <span>{"setTotalpeopleCount(data);"}</span>
            <br />
            <span>
              {
                "setPeople(data.slice((currentPage - 1) * leadsPerPage, currentPage * leadsPerPage));"
              }
            </span>
            <br />
            <span>{"const count = (data.length) / leadsPerPage;"}</span>
            <br />
            <span>{"if (count > 1) {"}</span>
            <br />
            <span>{"setTotalPages(Number.isInteger(count) ? count :"}</span>
            <br />
            <span>{"Math.trunc(count) + 1);"}</span>
            <br />
            <span>{"}"}</span>
            <br />
            <span>{"});"}</span>
            <br />
            <span>{"}, []);"}</span>
            <br />
            <span>{"const handlePaginatorChange = (newPageNumber) => {"}</span>
            <br />
            <span>{"setCurrentPage(newPageNumber);"}</span>
            <br />
            <span>
              {"newPageNumber === totalPages ? setPeople(totalpeopleCount."}
            </span>
            <br />
            <span>
              {
                "slice((newPageNumber - 1) * leadsPerPage, totalpeopleCount.length)) :"
              }
            </span>
            <br />
            <span>
              {
                "setPeople(totalpeopleCount.slice((newPageNumber - 1) * leadsPerPage,"
              }
            </span>
            <br />
            <span>{"newPageNumber * leadsPerPage));"}</span>
            <br />
            <span>{"}"}</span>
            <br />
            <span>{"return <>"}</span>
            <br />
            <span>{"<h1>User List</h1>"}</span>
            <br />
            <span>
              {"<"}div className="parentContainer"{">"}
            </span>
            <br />
            <span>
              {"<"}div className="personContainer"{">"}
            </span>
            <br />
            <span>{"{people.length > 0 && people.map((p, i) => {"}</span>
            <br />
            <span>{"return ("}</span>
            <br />
            <span>
              {"<"}div key={"{"}`personItem${"{"}i{"}"}`{"}"}{" "}
              className="personItem"{">"}
            </span>
            <br />
            <span>{"<img src={p.Image} />"}</span>
            <br />
            <span>{"<div key={`name${i}`}>{p.name}</div>"}</span>
            <br />
            <span>{"<div key={`phone${i}`}>{p.phone}</div>"}</span>
            <br />
            <span>{"<div key={`address${i}`}>{p.Address}</div>"}</span>
            <br />
            <span>{"</div>"}</span>
            <br />
            <span>{")"}</span>
            <br />
            <span>{"})}"}</span>
            <br />
            <span>{"</div>"}</span>
            <br />
            <span>{"<Paginator"}</span>
            <br />
            <span>{"currentPage={currentPage}"}</span>
            <br />
            <span>{"totalPages={totalPages}"}</span>
            <br />
            <span>{"handleChange={handlePaginatorChange}"}</span>
            <br />
            <span>{"/>"}</span>
            <br />
            <span>{"</div></>;"}</span>
            <br />
            <span>{"}"}</span>
            <br />
            <span>{"export default Search;"}</span>
          </pre>
        </CardContent>
      </Card>
      <Card style={{ marginBottom: 20 }}>
        <CardContent>
          <Typography
            variant="h6"
            component="div"
            style={{ borderBottom: "1px solid grey", marginBottom: 20 }}
          >
            paginator.js
          </Typography>
          <pre>
            <span>
              import {"{"} useState, useEffect {"}"} from "react";
            </span>
            <br />
            <span>
              const Paginator = {"({"} currentPage, totalPages, handleChange{" "}
              {"})"} ={">"} {"{"}
            </span>
            <br />
            <span>
              const {"["}inputValue, setInputValue{"]"} = useState{"("}""{")"};
            </span>
            <br />
            <span>
              useEffect{"(()"} ={">"} {"{"}
            </span>
            <br />
            <span>
              setInputValue{"("}currentPage?.toString{"())"};
            </span>
            <br />
            <span>
              {"}"}, {"["}currentPage{"])"};
            </span>
            <br />
            <span>
              {"const handleSubmit"} = {"(event)"} ={"{"}
            </span>
            <br />
            <span>{" event.preventDefault();"}</span>
            <br />
            <span>{" handleChange(Number.parseInt(inputValue) || 1);"}</span>
            <br />
            <span>{"}"};</span>
            <span>
              {"const handlePreviousClick = () =>"}
              {"{"}
            </span>
            <br />
            <span>{" handleChange(currentPage - 1 || 1);"}</span>
            <br />
            <span>{"}"}</span>
            <br />
            <span>
              {"const handleNextClick = () =>"} {"{"}
            </span>
            <br />
            <span>
              {
                " handleChange(currentPage + 1 <= totalPages ? currentPage + 1 : totalPages);"
              }
            </span>
            <br />
            <span>{"};"}</span>
            <br />
            <span>return {"("}</span>
            <br />
            <span>{"<>"}</span>
            <br />
            <span>
              {"<"}div className="paginatorContainer{">"}
            </span>
            <br />
            <span>{"<button"}</span>
            <br />
            <span>className="previous"</span>
            <br />
            <span>{"disabled={currentPage <= 1}"}</span>
            <br />
            <span>{"onClick={handlePreviousClick}"}</span>
            <br />
            <span>{">"}</span>
            <br />
            <span>{"<span></span>"}</span>
            <br />
            <span>{"</button>"}</span>
            <br />
            <span>
              {"<"}span className="inlineStyle"{">"}
            </span>
            <br />
            <span>
              {"<"}form onSubmit={"{"}handleSubmit{"}"}{" "}
              className="paginatorForm"{">"}
            </span>
            <br />
            <span>{"<input"}</span>
            <br />
            <span>aria-label="page number"</span>
            <br />
            <span>type="number"</span>
            <br />
            <span>min="1"</span>
            <br />
            <span>{" max={totalPages}"}</span>
            <br />
            <span>{"onChange={handleInputChange}"}</span>
            <br />
            <span>
              value={"{"}inputValue || ""{"}"}
            </span>
            <br />
            <span>className="redesignPaginatorInput"</span>
            <br />
            <span>{"/>"}</span>
            <br />
            <span>{"</form>"}</span>
            <br />
            <span>
              {"<"}span className="totalPages"{">"}of &nbsp;&nbsp;{"{"}
              totalPages{"}"}
              {"</span>"}
            </span>
            <br />
            <span>{" </span>"}</span>
            <br />
            <span>{" <button"}</span>
            <br />
            <span>className="next"</span>
            <br />
            <span>{"disabled={currentPage >= totalPages}"}</span>
            <br />
            <span>{"onClick={handleNextClick}"}</span>
            <br />
            <span>{">"}</span>
            <br />
            <span>{"<span></span>"}</span>
            <br />
            <span>{"</button>"}</span>
            <br />
            <span>{"</div>"}</span>
            <br />
            <span>{"</>"}</span>
            <br />
            <span>{");"}</span>
            <br />
            <span>{"};"}</span>
            <br />
            <span>{"export default Paginator;"}</span>
          </pre>
        </CardContent>
      </Card>{" "}
    </div>
  );
};
export default PaginatorPage;
