import { useState, useEffect } from "react";
import "./paginator.css";
import Arrow from "../../assets/chevron.svg";

const Paginator = ({ currentPage, totalPages, handleChange }) => {
  const [inputValue, setInputValue] = useState("");

  useEffect(() => {
    setInputValue(currentPage?.toString());
  }, [currentPage]);

  const handleInputChange = (event) => {
    setInputValue(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    handleChange(Number.parseInt(inputValue) || 1);
  };

  const handlePreviousClick = () => {
    handleChange(currentPage - 1 || 1);
  };

  const handleNextClick = () => {
    handleChange(currentPage + 1 <= totalPages ? currentPage + 1 : totalPages);
  };
  return (
    <>
      <div className="paginatorContainer">
        <button
          className="previous"
          disabled={currentPage <= 1}
          onClick={handlePreviousClick}
        >
          <span></span>
        </button>

        <span className="inlineStyle">
          <form onSubmit={handleSubmit} className="paginatorForm">
            <input
              aria-label="page number"
              type="number"
              min="1"
              max={totalPages}
              onChange={handleInputChange}
              value={inputValue || ""}
              className="redesignPaginatorInput"
            />
          </form>
          <span className="totalPages">of &nbsp;&nbsp;{totalPages}</span>
        </span>
        <button
          className="next"
          disabled={currentPage >= totalPages}
          onClick={handleNextClick}
        >
          <span></span>
        </button>
      </div>
    </>
  );
};

export default Paginator;
