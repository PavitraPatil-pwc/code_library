import Typography from "@mui/material/Typography";
import Cards from "../Cards/Cards";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import accordian1 from "../Assets/accordian-1.png";
import accordian2 from "../Assets/accordian-2.png";
import AccordianCode from "./accordianCode";
import { CardContent } from "@mui/material";
import Card from "@mui/material/Card";

const Accordian = () => {
  return (
    <div>
      <Typography variant="h5" component="div">
        Accordian
      </Typography>
      <Typography component="div" style={{ margin: 8 }}>
        Accordion lists can be added and managed by adding the Accordion item to
        your column through the Component Type menu.
      </Typography>
      <Typography
        variant="h6"
        component="div"
        style={{ borderBottom: "1px solid grey", marginBottom: 20 }}
      >
        Layout
      </Typography>
      <Box sx={{ flexGrow: 1 }} style={{ marginTop: 20, marginBottom: 10 }}>
        <Grid container style={{ marginBottom: 10 }}>
          <Grid item xs={8}>
            <img src={accordian2} alt="datepicker" height="200" width="800" />
          </Grid>
        </Grid>
        <Grid container>
          <Grid item xs={4}>
            <img src={accordian1} alt="datepicker" height="300" width="800" />
          </Grid>
        </Grid>
      </Box>
      <Typography component="div" style={{ marginBottom: 20 }}>
        Cascading dropdown is a group of dropdowns where the value of one
        dropdown depends upon another dropdown value. Child dropdown values are
        populated based on the item selected in the parent dropdown. We should
        have basic knowledge of React. js and Web API.
      </Typography>
      <Typography
        variant="h6"
        component="div"
        style={{ borderBottom: "1px solid grey", marginBottom: 20 }}
      >
        Default
      </Typography>{" "}
      <Typography component="div" style={{ marginBottom: 20 }}>
        The user can click on the arrow to view the data.
      </Typography>
      <Card style={{ marginBottom: 20, background: "grey" }}>
        <CardContent>
          <AccordianCode />
        </CardContent>
      </Card>
      <Typography
        variant="h6"
        component="div"
        style={{ borderBottom: "1px solid grey", marginBottom: 20 }}
      >
        Instruction
      </Typography>
      <Typography component="div" style={{ marginBottom: 20 }}>
        <pre>
          <span>1. install the library</span>
          <br />
          <span>
            2.import Accordian hook. you can pass props like title, defaultOpen,
            fullWidth
          </span>
          <br />
          <span>
            3.then pass the children{"("}
            {"<"}Accordian{">"}
            {"<"}div{">"}here goes your dom node{"<"}/div{">"}
            {"<"}/Accordian{">"}
            {")"}
          </span>
          <br />
          <span>4.no extra set up needed</span>
          <br />
          <span>
            5.click on the link to see the demo implementation -{">"}{" "}
            {"https://codesandbox.io/s/accordian-9vsnwk?file=/src/App.js"}
          </span>
        </pre>
      </Typography>
      <Typography
        variant="h6"
        component="div"
        style={{ borderBottom: "1px solid grey", marginBottom: 20 }}
      >
        Usage
      </Typography>
      <Card style={{ marginBottom: 20 }}>
        <CardContent>
          <Typography
            variant="h6"
            component="div"
            style={{ borderBottom: "1px solid grey", marginBottom: 20 }}
          >
            App.js
          </Typography>
          <pre>
            <span>import AccordionComponent from "./accordianComponent";</span>
            <br />
            <span>
              const App = () ={">"} {"{"}
            </span>
            <br />
            <span>return {"("}</span>
            <br />
            <span>
              {"<"}div{">"}
            </span>
            <br />
            <span>
              {"<"}div style={"{{"}
              display: "flex", flexDirection: "column", alignItems: "center",
              {"}}"}
              {">"}
            </span>
            <br />
            <span>
              {"<"}AccordionComponent title="Custom accordian closed state"
              defaultOpen={"{"}true{"}"}
              {">"}
            </span>
            <br />
            <span>
              {"<"}div{">"}
            </span>
            <br />
            <span>
              Lorem Ipsum is simply dummy text of the printing and typesetting
            </span>
            <br />
            <span>
              industry. Lorem Ipsum has been the industry's standard dummy text
            </span>
            <br />
            <span>
              ever since the 1500s, when an unknown printer took a galley of
              type
            </span>
            <br />
            <span>and scrambled it to make a type specimen book</span>
            <br />
            <span>
              {"<"}/div{">"};
            </span>
            <br />
            <span>
              {"<"}/AccordionComponent{">"}
            </span>
            <br />
            <span>
              {"<"}AccordionComponent title="Custom accordian" defaultOpen={"{"}
              false{"}"}
              fullWidth
              {">"}
            </span>
            <br />
            <span>
              {"<"}div{">"}
            </span>
            <br />
            <span>
              Lorem Ipsum is simply dummy text of the printing and typesetting
            </span>
            <br />
            <span>
              industry. Lorem Ipsum has been the industry's standard dummy text
            </span>
            <br />
            <span>
              ever since the 1500s, when an unknown printer took a galley of
              type
            </span>
            <br />
            <span>and scrambled it to make a type specimen book</span>
            <br />
            <span>
              {"<"}/div{">"};
            </span>
            <br />
            <span>
              {"<"}AccordionComponent{">"};
            </span>
            <br />
            <span>
              {"<"}/div{">"};
            </span>
            <br />
            <span>
              {"<"}/div{">"};
            </span>
            <br />
            <span>
              {")"};{"}"}
            </span>
            <br />
            <span>export default App;</span>
            <br />
          </pre>
        </CardContent>
      </Card>{" "}
      <Card style={{ marginBottom: 20 }}>
        <CardContent>
          <Typography
            variant="h6"
            component="div"
            style={{ borderBottom: "1px solid grey", marginBottom: 20 }}
          >
            accordianComponent.jsx
          </Typography>
          <pre>
            <span>
              import React, {"{"} useState {"}"} from "react";
            </span>
            <br />
            <span>
              const AccordionComponent = {"({"}title = "", children, defaultOpen
              = false,
            </span>
            <br />
            <span>
              fullWidth = false,
              {"}) => {"}
            </span>
            <br />
            <span>
              const {"["}isOpen, setOpen{"]"} = useState{"("}defaultOpen{")"};
            </span>
            <br />
            <span>return {" ("}</span>
            <br />
            <span>{"<"}div</span>
            <br />
            <span>data-testid="accordian"</span>
            <br />
            <span>
              className={"{["}styles.accordionwrapper, fullWidth {"&&"}{" "}
              styles.fullWidth{"]"}.join{"("}" "
            </span>
            <br />
            <span>
              {")}"}
              {" >"}
            </span>
            <br />
            <span>{"<"}div</span>
            <br />
            <span>
              className={"{["}styles.accordiontitle, isOpen {"?"} styles.open :
              ""
              {"]"}.join{"("}" "{")}"}
            </span>
            <span>
              onClick={"{()"} ={">"} setOpen{"("}!isOpen{")}"}
            </span>
            <br />
            <span>{">"}</span>
            <br />
            <span>{"{title}"}</span>
            <br />
            <span>
              {"<"}/div{">"}
            </span>
            <br />
            <span>{"<div"}</span>
            <br />
            <span>
              className={"{["}styles.accordionitem, !isOpen ? styles.collapsed :
              ""{"]"}.join{"("}" "
            </span>
            <br />
            <span>
              {" "}
              {")}"}
              {">"}
            </span>
            <br />
            <span>
              {" "}
              {"<"}div className={"{"}styles.accordioncontent{"}"}
              {">"}
              {"{children}"}
              {"</div>"}
            </span>
            <br />
            <span>{" </div>"}</span>
            <br />
            <span>
              {"<"}/div{">"}
            </span>
            <br />
            <span>
              {");"}
              {"};"}
            </span>
            <br />
            <span>export default AccordionComponent;</span>
            <br />
          </pre>
        </CardContent>
      </Card>
    </div>
  );
};
export default Accordian;
