import React from "react";
import AccordionComponent from "./accordianComponent";

const AccordianCode = () => {
  return (
    <div>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <AccordionComponent
          title="Custom accordian closed state"
          defaultOpen={true}
        >
          <div>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book
          </div>
        </AccordionComponent>
        <AccordionComponent
          title="Custom accordian"
          defaultOpen={false}
          fullWidth
        >
          <div>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book
          </div>
        </AccordionComponent>
      </div>
    </div>
  );
};

export default AccordianCode;
