import React, { useState } from "react";
import styles from "./style.module.css";

const AccordionComponent = ({
  title = "",
  children,
  defaultOpen = false,
  fullWidth = false,
}) => {
  const [isOpen, setOpen] = useState(defaultOpen);
  return (
    <div
      data-testid="accordian"
      className={[styles.accordionwrapper, fullWidth && styles.fullWidth].join(
        " "
      )}
    >
      <div
        className={[styles.accordiontitle, isOpen ? styles.open : ""].join(" ")}
        onClick={() => setOpen(!isOpen)}
      >
        {title}
      </div>
      <div
        className={[styles.accordionitem, !isOpen ? styles.collapsed : ""].join(
          " "
        )}
      >
        <div className={styles.accordioncontent}>{children}</div>
      </div>
    </div>
  );
};
export default AccordionComponent;
