import Typography from "@mui/material/Typography";
import Cards from "../Cards/Cards";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import cascaded1 from "../Assets/cascaded1.png";
import cascaded2 from "../Assets/cascaded2.png";
import { CardContent } from "@mui/material";
import Card from "@mui/material/Card";
import DropdownCode from "./DropdownCode";

const Dropdown = () => {
  return (
    <div>
      <Typography variant="h5" component="div">
        Dropdown
      </Typography>
      <Typography component="div" style={{ margin: 8 }}>
        A React component which provides multi select functionality with various
        features like selection limit, CSS customization, checkbox, search
        option, disable preselected values, flat array, keyboard navigation for
        accessibility and grouping features. Also it has feature to behave like
        normal dropdown(means single select dropdown).
      </Typography>
      <Typography
        variant="h6"
        component="div"
        style={{ borderBottom: "1px solid grey", marginBottom: 20 }}
      >
        Installation
      </Typography>
      <Typography style={{ marginBottom: 30 }}>
        <Cards container="npm install antd" />
      </Typography>
      <Typography
        variant="h6"
        component="div"
        style={{ borderBottom: "1px solid grey", marginBottom: 20 }}
      >
        Layout
      </Typography>
      <Box sx={{ flexGrow: 1 }} style={{ marginTop: 20, marginBottom: 10 }}>
        <Grid container style={{ marginBottom: 10 }}>
          <Grid item xs={8}>
            <img src={cascaded1} alt="datepicker" height="300" width="800" />
          </Grid>
        </Grid>
        <Grid container>
          <Grid item xs={4}>
            <img src={cascaded2} alt="datepicker" height="150" width="400" />
          </Grid>
        </Grid>
      </Box>
      <Typography component="div" style={{ marginBottom: 20 }}>
        Cascading dropdown is a group of dropdowns where the value of one
        dropdown depends upon another dropdown value. Child dropdown values are
        populated based on the item selected in the parent dropdown. We should
        have basic knowledge of React. js and Web API.
      </Typography>
      <Typography
        variant="h6"
        component="div"
        style={{ borderBottom: "1px solid grey", marginBottom: 20 }}
      >
        Default
      </Typography>{" "}
      <Typography component="div" style={{ marginBottom: 20 }}>
        The user can select multiple dates.
      </Typography>
      <Card style={{ marginBottom: 20, background: "grey" }}>
        <CardContent>
          <DropdownCode />
        </CardContent>
      </Card>
      <Typography
        variant="h6"
        component="div"
        style={{ borderBottom: "1px solid grey", marginBottom: 20 }}
      >
        Instruction
      </Typography>
      <Typography component="div" style={{ marginBottom: 20 }}>
        <pre>
          <span>1. install the library</span>
          <br />
          <span>
            2.import Accordian hook. you can pass props like title, defaultOpen,
            fullWidth
          </span>
          <br />
          <span>
            3.then pass the children{"("}
            {"<"}Accordian{">"}
            {"<"}div{">"}here goes your dom node{"<"}/div{">"}
            {"<"}/Accordian{">"}
            {")"}
          </span>
          <br />
          <span>4.no extra set up needed</span>
          <br />
          <span>
            5.click on the link to see the demo implementation -{">"}{" "}
            {"https://codesandbox.io/s/accordian-9vsnwk?file=/src/App.js"}
          </span>
        </pre>
      </Typography>
      <Typography
        variant="h6"
        component="div"
        style={{ borderBottom: "1px solid grey", marginBottom: 20 }}
      >
        Usage
      </Typography>
      <Card style={{ marginBottom: 20 }}>
        <CardContent>
          <Typography
            variant="h6"
            component="div"
            style={{ borderBottom: "1px solid grey", marginBottom: 20 }}
          >
            App.js
          </Typography>
          <pre>
            <span>import "antd/dist/antd.css";</span>
            <br />
            <span>import CascadedSelectBox from "./cascaded";</span>
            <br />
            <span>
              function App{"()"} {"{"}
            </span>
            <br />
            <span>
              const options = {"["}
              {" {"}
              label: "Light", value: "light",
            </span>
            <br />
            <span>
              children: new Array{"(20)"}
              .fill{"(null)"}
              .map{"((_, index)"} ={">"}{" "}
            </span>
            <br />
            <span>
              {"({ label: `Number ${index}`, value: index }))"},{"}"},
            </span>
            <br />
            <span> label: "Bamboo", value: "bamboo",</span>
            <br />
            <span>{" children: ["}</span>
            <br />
            <span>
              {"{"} label: "Toy Fish", value: "fish",
              {"}"}
            </span>
            <br />
            <span>
              {"{"} label: "Toy Cards", value: "cards",
              {"}"}
            </span>
            <span>
              {"{"} label: "Toy Bird", value: "bird",
              {"}"}
            </span>
            <br />
            <span>
              {"]"},{"}"},{"]"},{"}"},{"]"};
            </span>
            <br />
            <span>{"const onChange = (value) => {"}</span>
            <br />
            <span>{"console.log(value);"}</span>
            <br />
            <span>{"};"}</span>
            <br />
            <span>{"return ("}</span>
            <br />
            <span>
              {"<"}div{">"}
            </span>
            <br />
            <span>
              {"<CascadedSelectBox onChange={onChange} options={options} />"}
            </span>
            <br />
            <span>{"</div>"}</span>
            <br />
            <span>
              {")"};{"}"}
            </span>
            <br />
            <span>export default App;</span>
            <br />
          </pre>
        </CardContent>
      </Card>{" "}
      <Card style={{ marginBottom: 20 }}>
        <CardContent>
          <Typography
            variant="h6"
            component="div"
            style={{ borderBottom: "1px solid grey", marginBottom: 20 }}
          >
            Cascad.js
          </Typography>
          <pre>
            <span>import React from "react";</span>
            <br />
            <span>import {"{ Cascader }"} from "antd";</span>
            <br />
            <span>
              const CascadedSelectBox ={"(props)"} {"{"}
            </span>
            <br />
            <span>const {" {"}</span>
            <br />
            <span>
              () onChange = {"()"} ={">"}
              {"{}"},
            </span>
            <br />
            <span>{"options = [],"},</span>
            <br />
            <span> label = "Select Suitable Value",</span>
            <br />
            <span>multiple = true,</span>
            <br />
            <span>{"}"} = props;</span>
            <br />
            <span>return {"("}</span>
            <span>
              {"<"}div{">"}
            </span>
            <br />
            <span>
              {"<"}p{">"},{"{"}label{"}"};
            </span>
            <br />
            <span>{"<Cascader"}</span>
            <br />
            <span>
              style={"{"}
              {"{"} width: {"100%"} {"}"}
              {"}"}
            </span>
            <br />
            <span>placeholder="Choose muliple"</span>
            <br />
            <span>{"options={options}"}</span>
            <br />
            <span>{"onChange={onChange}"}</span>
            <br />
            <span>{"multiple={multiple}"}</span>
            <br />
            <span>maxTagCount="responsive"</span>
            <br />
            <span>{"/>"}</span>
            <br />
            <span>
              {"<"}div{">"}
            </span>
            <br />

            <span>
              {");"}
              {"};"}
            </span>
            <br />
            <span>export default CascadedSelectBox;</span>
            <br />
          </pre>
        </CardContent>
      </Card>
    </div>
  );
};
export default Dropdown;
