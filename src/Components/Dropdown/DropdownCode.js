import "antd/dist/antd.css";
import { Cascader } from "antd";
import CascadedSelectBox from "./DropdownCascad";

function DropdownCode() {
  const options = [
    {
      label: "Light",
      value: "light",
      children: new Array(20)
        .fill(null)
        .map((_, index) => ({ label: `Number ${index}`, value: index })),
    },
    {
      label: "Bamboo",
      value: "bamboo",
      children: [
        {
          label: "Little",
          value: "little",
          children: [
            {
              label: "Toy Fish",
              value: "fish",
            },
            {
              label: "Toy Cards",
              value: "cards",
            },
            {
              label: "Toy Bird",
              value: "bird",
            },
          ],
        },
      ],
    },
  ];

  const onChange = (value) => {
    console.log(value);
  };
  return (
    <div>
      <CascadedSelectBox onChange={onChange} options={options} />
    </div>
  );
}

export default DropdownCode;
