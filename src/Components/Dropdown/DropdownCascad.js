import { Cascader } from "antd";
import React from "react";

const CascadedSelectBox = (props) => {
  const {
    onChange = () => {},
    options = [],
    label = "Select Suitable Value",
    multiple = true,
  } = props;

  return (
    <div className="cascadedDiv">
      <p>{label}</p>
      <Cascader
        style={{ width: "100%" }}
        placeholder="Choose muliple"
        options={options}
        onChange={onChange}
        multiple={multiple}
        maxTagCount="responsive"
      />
    </div>
  );
};

export default CascadedSelectBox;
