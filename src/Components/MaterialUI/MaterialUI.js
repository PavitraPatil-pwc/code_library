import Typography from "@mui/material/Typography";
import Cards from "../Cards/Cards";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Toolbar from "../Assets/Toolbar.png";
import Tab from "../Assets/Tab.PNG";
import InputUsedForm from "../Assets/InputUsedForm.PNG";
import InputsUsedInFormsWithValidation from "../Assets/InputsUsedInFormsWithValidation.png";
import { CardContent } from "@mui/material";
import Card from "@mui/material/Card";
// import MaterialUICode from "./MaterialUICode";

const MaterialUI = () => {
  return (
    <div>
      <Typography variant="h5" component="div">
        custom inputs with react material {"&"} validations
      </Typography>
      <Typography component="div" style={{ margin: 8 }}>
        Material UI is a comprehensive library of components that features our
        implementation of Google's Material Design system.
      </Typography>
      <Typography style={{ marginBottom: 30 }}>
        <Cards container="npm i @mui/material" />
      </Typography>
      <Typography
        variant="h6"
        component="div"
        style={{ borderBottom: "1px solid grey", marginBottom: 20 }}
      >
        Layout
      </Typography>
      <Box sx={{ flexGrow: 1 }} style={{ marginTop: 20, marginBottom: 10 }}>
        <Typography component="div" style={{ marginBottom: 5 }}>
          Toolbar
        </Typography>
        <Grid container style={{ marginBottom: 10 }}>
          <Grid item xs={10}>
            <img src={Toolbar} alt="datepicker" height="100" width="850" />
          </Grid>
        </Grid>
        <Typography component="div" style={{ marginBottom: 5 }}>
          Tab
        </Typography>
        <Grid container style={{ marginBottom: 10 }}>
          <Grid item xs={10}>
            <img src={Tab} alt="datepicker" height="100" width="850" />
          </Grid>
        </Grid>
        <Typography component="div" style={{ marginBottom: 5 }}>
          Input Used Form
        </Typography>
        <Grid container style={{ marginBottom: 10 }}>
          <Grid item xs={10}>
            <img
              src={InputUsedForm}
              alt="datepicker"
              height="200"
              width="850"
            />
          </Grid>
        </Grid>
        <Typography component="div" style={{ marginBottom: 5 }}>
          Inputs Used Forms With Validation
        </Typography>
        <Grid container style={{ marginBottom: 10 }}>
          <Grid item xs={10}>
            <img
              src={InputsUsedInFormsWithValidation}
              alt="datepicker"
              height="200"
              width="850"
            />
          </Grid>
        </Grid>
      </Box>
      <Typography component="div" style={{ marginBottom: 20 }}>
        MUI offers a comprehensive suite of UI tools to help you ship new
        features faster. Start with Material UI, our fully-loaded component
        library, or bring your own design system to our production-ready
        components.
      </Typography>
      <Typography
        variant="h6"
        component="div"
        style={{ borderBottom: "1px solid grey", marginBottom: 20 }}
      >
        Instruction
      </Typography>{" "}
      <Typography component="div" style={{ marginBottom: 20 }}>
        <pre>
          <span>1. For custom Inputs form install the plugins </span>
          <br />
          <span>{"   "}npm install "@date-io/date-fns"</span>
          <br />
          <span>{"   "}npm install "@material-ui/core"</span>
          <br />
          <span>{"   "}npm install "@material-ui/icons"</span>
          <br />
          <span>{"   "}npm install "@material-ui/lab"</span>
          <br />
          <span>{"   "}npm install "@material-ui/pickers"</span>
          <br />
          <span>{"   "}npm install "@material-ui/styles"</span>
          <br />
          <span>{"   "}npm install "@mdi/js"</span>
          <br />
          <span>{"   "}npm install "@mdi/react"</span>
          <br />
          <span>{"   "}npm install "classnames"</span>
          <br />
          <span>{"   "}npm install "date-fns"</span>
          <br />
          <span>{"   "}npm install "formik"</span>
          <br />
          <span>{"   "}npm install "mui-datatables"</span>
          <br />
          <span>{"   "}npm install "prop-types"</span>
          <br />
          <span>{"   "}npm install "yup"</span>
          <br />
          <span>
            2. For using Custom tab components Please refer below command
          </span>
          <br />
          <span>{"   "}npm install react material packages</span>
          <br />
          <span>3. For using custom toolbar component Please refer below</span>
          <br />
          <span>{"   "}npm install react material packages</span>
        </pre>
      </Typography>
    </div>
  );
};
export default MaterialUI;
