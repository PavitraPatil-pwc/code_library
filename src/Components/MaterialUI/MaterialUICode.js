import { CustomPaper } from "./MaterialComponent/Paper";
import MyForm from "./MaterialComponent/MyForm";
import MyTabs from "./MaterialComponent/MyTabs";
import MatToolbar from "./MaterialComponent/Toolbar";

function MaterialUICode() {
  return (
    <div>
      <CustomPaper>
        <MyForm />
        <MyTabs />
        <MatToolbar>Sourish Toolbar</MatToolbar>
      </CustomPaper>
    </div>
  );
}

export default MaterialUICode;
