import React from 'react'
import MatTab from  './Tab'
import TabPanel from './Tab/TabPanel'

const tablist = [
    {id:'home',label:'Home'},
    {id:'dashboard',label:'Dashboard'}
]

const MyTabs = () => {
  return (
    <>
            <MatTab TabsHeaderList={tablist} color="accent">
                {({value}) => (
                    <>
                        <TabPanel value={value} index={value}>
                                part - {tablist[value].label}
                        </TabPanel>
                    </>
                )}
            </MatTab>
    </>
  )
}

export default MyTabs